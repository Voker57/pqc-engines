#include "params.h"
#include "openssl/rand.h"
#include "api.h"

#define STR(a) #a

// engine id, string
char *engine_id = STR(PQC_ENGINES_ID);
// algorithm name, in machine friendly form, i.e. "ZEA-256"
char *alg_name = CRYPTO_ALGNAME;
// algorithm OID
char *alg_oid = STR(PQC_ENGINES_OID);

// size of secret key
size_t pkey_sk_size = CRYPTO_SECRETKEYBYTES;
// size of public key
size_t pkey_pk_size = CRYPTO_PUBLICKEYBYTES;

#ifdef PQC_ENGINES_SIGN
	#define KEYGEN crypto_sign_keypair
#endif
#ifdef PQC_ENGINES_ENC
	#define KEYGEN crypto_encrypt_keypair
#endif
#ifdef PQC_ENGINES_KEM
	#define KEYGEN crypto_kem_keypair
#endif

int pkey_generate_keypair(unsigned char* pk, unsigned char* sk) {
	KEYGEN(pk, sk);
	return 1;
}

#ifdef PQC_ENGINES_SIGN

size_t pkey_signature_size(size_t mlen) {
	return mlen + CRYPTO_BYTES;
}

// sign a message
int pkey_sign(unsigned char *sig, size_t *siglen, const unsigned char *m, size_t mlen, const unsigned char *sk) {
	if(*siglen < pkey_signature_size(mlen)) {
		// Not enough
		return -1;
	}
	unsigned long long siglen_ull = *siglen;
	*siglen = siglen_ull;
	return crypto_sign(sig, &siglen_ull, m, mlen, sk) >= 0;	
}
// verify a message
int pkey_verify(unsigned char *sig, size_t siglen, const unsigned char *m, size_t mlen, const unsigned char *pk) {
	char *m = OPENSSL_malloc(siglen);
	unsigned long long mlen;
	int r = crypto_sign_open(m, &mlen, sig, siglen, pk);
	OPENSSL_free(m);
	return r >= 0;
}

#endif

#ifdef PQC_ENGINES_KEM

size_t kem_encrypted_key_size = CRYPTO_CIPHERTEXTBYTES;
// size of key being encapsulated
size_t kem_secret_size = CRYPTO_BYTES;
// turn given data into key and ct from which the key can be restored
int kem_encapsulate(unsigned char *ct, unsigned char *ss, const unsigned char *pk) {
	int r = crypto_kem_enc(ct, ss, pk);
	return r >= 0;
}
// turn given ciphertext back into key
int kem_decapsulate(unsigned char *ss, const unsigned char *ct, const unsigned char *sk) {
	int r = crypto_kem_dec(ss, ct, sk);
	return r >= 0;
}

#endif

void randombytes(unsigned char *x, unsigned long long xlen) {
	RAND_bytes(x, xlen);
}
